#!/bin/bash
set -ex

for i in _projects/*.html; do
	NAME="$(cat $i | grep '^name:' | sed -e 's/name: \+//g')"
	for j in status checks; do
		DATA="$(cat $i | grep "^$j:" | sed -e "s/$j: \+//g")"
		if [[ $DATA == "passing" ]]; then
			COLOR=green
		else
			COLOR=red
		fi
		echo "fetching badge $NAME, $j is $DATA, color is $COLOR"
		curl https://img.shields.io/badge/$j-${DATA}-${COLOR} -o _projects/${NAME}_${j}.svg
	done
done
	
