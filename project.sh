#!/bin/bash

get_project() {
	cat _projects/${1}.html | grep "$2" | sed -e "s#$2: ##g"
}

set_project() {
	sed -e "s#$2:.*#$2: $3#g" -i _projects/${1}.html
}

pull_project() {
	git pull 1>project.log 2>&1
}

push_project() {
	git commit -m "${project}: reserved build number $BUILDNR" _projects/${project}.html 1>>project.log 2>&1
	git push 1>>project.log 2>&1
}

if [[ -z $1 ]]; then
	cmd=help
else
	cmd=$1; shift
fi
if [[ $cmd != help ]]; then
	project=$1; shift
	pull_project
fi

case $cmd in
	build)
		set_project $project status $1; shift
		if [[ -n $1 ]]; then
			set_project $project checks $1; shift
		fi
		if [[ -n $1 ]]; then
			set_project $project latest $1; shift
		fi
		set_project $project date "$(date '+%F %T')"
		push_project
	;;
	number)
		BUILDNR=$(( $(get_project ${project} buildnr) + 1 ))
		set_project $project buildnr $BUILDNR 1>>project.log 2>&1
		push_project
		echo "export BUILDNR=$BUILDNR"
	;;
	create)
		NR=$3; if [[ -z $NR ]]; then NR=1; fi
		cat >_projects/${project}.html <<-EOF
---
name: $project
title: $1
project: $2
latest:
buildnr: $NR
status:
checks:
date:
---
		EOF
		push_project
	;;
	help)
		cat <<-EOF
		
		Usage: project.sh <command> [<options>]
		
		./project.sh build <project> <passing/failed> [<ok/failed> [<latest URL>]]
		
		./project.sh number <project>

		./project.sh create <project> <title> <project URL> [<nr>]

		EOF
	;;
esac
